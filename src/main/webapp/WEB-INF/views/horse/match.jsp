<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>

<style>
  table {
    width: 800px;
    border-top: 1px solid #444444;
    border-collapse: collapse;
  }
  th, td {
    border-bottom: 1px solid #444444;
    padding: 10px;
  }
</style>

<html>
<head>
<title>한줄코멘트</title>
</head>
<body>
	<h2> 한글이 제대로 나와야함.</h2>
	<ul>
		<li><a href="/horse/match">경기 기록</a></li>
		<li><a href="/horse/horseInfo">경주마 정보</a></li>
		<li><a href="/horse/jkInfo">기수 정보</a></li>
		<li>
			<p>로그인 버튼 나중에 만들기</p>
<%-- 			<c:choose>
			    <c:when test="${sessionScope.id == null}">
			        <a href="${path}/member/login.do">로그인</a>
			    </c:when>
			    <c:otherwise>
			        ${sessionScope.name}님이 로그인중입니다.
			        <a href="${path}/member/logout.do">로그아웃</a>
			    </c:otherwise>
			</c:choose> --%>
		</li>
	
	</ul>	
	<!-- 보드 -->
	<table>
		<c:forEach items="${MatchList}" var="resultInfo" varStatus="status">
			<tr>
				<td>${resultInfo.rcDate }</td>
				<td>${resultInfo.rcNo}</td>
				<td>${resultInfo.hrName}</td>
				<td>${resultInfo.hrNo}</td>
				<td>${resultInfo.jkName}</td>
				<td>${resultInfo.jkNo}</td>
				<td>${resultInfo.meet}</td>
				<td>${resultInfo.prd}</td>
			</tr>
		</c:forEach>
	</table>
	
</body>
</html>