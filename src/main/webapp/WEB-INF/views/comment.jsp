<%@ page contentType="text/html; charset=utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ page session="false" %>

<html>
<head>
<title>한줄코멘트</title>
</head>
<body>
	<h2> 한글이 제대로 나와야함.</h2>
	<!-- 보드 -->
	<table>
		<c:forEach items="${CommentList}" var="resultInfo" varStatus="status">
			<tr>
				<td>${resultInfo.connet_no }</td>
				<td>${resultInfo.text}</td>
				<td>${resultInfo.datetime}</td>
			</tr>
		</c:forEach>
	</table>
	
	<!-- 입력 -->
	<form id="comment-insert" method="post" action="/myapp/SimpleComment/Insert/">
		<input type="text" name="text" placeholder="내용"/>
		<input type="submit" value="입력" />
	</form>
	
</body>
</html>