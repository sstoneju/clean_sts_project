package com.first.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "simple_comment")
public class SimpleComment implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name ="comment_no")
	private long connet_no;
	
	@Column(nullable = false)
	private String text;
	
	@Column(insertable = false, updatable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date datetime;
	
	
	// 아아 여기는 public 이어야 하는구나.? 아닌데 private으로 해도 될텐데.
	public long getConnet_no() {
		return connet_no;
	}

	public void setConnet_no(long connet_no) {
		this.connet_no = connet_no;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}


	



}
