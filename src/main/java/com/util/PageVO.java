package com.util;

import java.io.Serializable;
import java.util.Date;

public class PageVO implements Serializable{

	private static final long serialVersionUID = 1L;
    private String searchCondition = "";
    private String searchKeyword = "";
    private String searchCondition2 = "";
    private String searchKeyword2 = "";
    private String searchCondition3 = "";
    private String searchKeyword3 = "";
    private String searchCondition4 = "";
    private String searchKeyword4 = "";
    private String searchCondition5 = "";
    private String searchKeyword5 = "";
    private String searchDateCondition = "";
    private Date searchStartDate = null;
    private Date searchEndDate = null;
    private int pageIndex = 1;
    private int pageUnit = 10;
    private int pageSize = 10;
    private int firstIndex = 1;
    private int lastIndex = 1;
    private int recordCountPerPage = 10;
    
    
	public String getSearchCondition() {
		return searchCondition;
	}
	public void setSearchCondition(String searchCondition) {
		this.searchCondition = searchCondition;
	}
	public String getSearchKeyword() {
		return searchKeyword;
	}
	public void setSearchKeyword(String searchKeyword) {
		this.searchKeyword = searchKeyword;
	}
	public String getSearchCondition2() {
		return searchCondition2;
	}
	public void setSearchCondition2(String searchCondition2) {
		this.searchCondition2 = searchCondition2;
	}
	public String getSearchKeyword2() {
		return searchKeyword2;
	}
	public void setSearchKeyword2(String searchKeyword2) {
		this.searchKeyword2 = searchKeyword2;
	}
	public String getSearchCondition3() {
		return searchCondition3;
	}
	public void setSearchCondition3(String searchCondition3) {
		this.searchCondition3 = searchCondition3;
	}
	public String getSearchKeyword3() {
		return searchKeyword3;
	}
	public void setSearchKeyword3(String searchKeyword3) {
		this.searchKeyword3 = searchKeyword3;
	}
	public String getSearchCondition4() {
		return searchCondition4;
	}
	public void setSearchCondition4(String searchCondition4) {
		this.searchCondition4 = searchCondition4;
	}
	public String getSearchKeyword4() {
		return searchKeyword4;
	}
	public void setSearchKeyword4(String searchKeyword4) {
		this.searchKeyword4 = searchKeyword4;
	}
	public String getSearchCondition5() {
		return searchCondition5;
	}
	public void setSearchCondition5(String searchCondition5) {
		this.searchCondition5 = searchCondition5;
	}
	public String getSearchKeyword5() {
		return searchKeyword5;
	}
	public void setSearchKeyword5(String searchKeyword5) {
		this.searchKeyword5 = searchKeyword5;
	}
	public String getSearchDateCondition() {
		return searchDateCondition;
	}
	public void setSearchDateCondition(String searchDateCondition) {
		this.searchDateCondition = searchDateCondition;
	}
	public Date getSearchStartDate() {
		return searchStartDate;
	}
	public void setSearchStartDate(Date searchStartDate) {
		this.searchStartDate = searchStartDate;
	}
	public Date getSearchEndDate() {
		return searchEndDate;
	}
	public void setSearchEndDate(Date searchEndDate) {
		this.searchEndDate = searchEndDate;
	}
	public int getPageIndex() {
		return pageIndex;
	}
	public void setPageIndex(int pageIndex) {
		this.pageIndex = pageIndex;
	}
	public int getPageUnit() {
		return pageUnit;
	}
	public void setPageUnit(int pageUnit) {
		this.pageUnit = pageUnit;
	}
	public int getPageSize() {
		return pageSize;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public int getFirstIndex() {
		return firstIndex;
	}
	public void setFirstIndex(int firstIndex) {
		this.firstIndex = firstIndex;
	}
	public int getLastIndex() {
		return lastIndex;
	}
	public void setLastIndex(int lastIndex) {
		this.lastIndex = lastIndex;
	}
	public int getRecordCountPerPage() {
		return recordCountPerPage;
	}
	public void setRecordCountPerPage(int recordCountPerPage) {
		this.recordCountPerPage = recordCountPerPage;
	}
 	
 	
}
