package horse.match.jpa;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.util.PageVO;

@Entity
@Table(name = "horse_match_info")
public class HorseMatch extends PageVO {
	
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name ="match_id")
	private long match_id;
	
	@Column(nullable = true)
	private int age;
	
	@Column(nullable = true)
	private int chaksunT;
	
	@Column(nullable = true)
	private int chaksunY;
	
	@Column(nullable = true)
	private int chaksun_6m;
	
	@Column(nullable = true)
	private int chulNo;
	
	@Column(nullable = false)
	private String hrName;
	
	@Column(nullable = true)
	private int hrNo;
	
	@Column(nullable = false)
	private String jkName ;
	
	@Column(nullable = true)
	private int jkNo ;
	
	@Column(nullable = false)
	private String meet;
	
	@Column(nullable = true)
	private int ord1CntT;
	
	@Column(nullable = true)
	private int ord1CntY;
	
	@Column(nullable = true)
	private int ord2CntT;
	
	@Column(nullable = true)
	private int ord2CntY;
	
	@Column(nullable = false)
	private String owName;
	
	@Column(nullable = true)
	private int owNo;
	
	@Column(nullable = false)
	private String prd;
	
	@Column(nullable = false)
	private String rating;
	
	@Column(nullable = true)
	private int rcCntT;
	
	@Column(nullable = true)
	private int rcCntY;
	
	@Temporal(TemporalType.TIMESTAMP)
	private Date rcDate;
	
	@Column(nullable = true)
	private int rcNo;
	
	@Column(nullable = false)
	private String sex;
	
	@Column(nullable = false)
	private String trName;
	
	@Column(nullable = true)
	private int trNo;
	
	@Column(nullable = true)
	private int wgBudam;

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public int getChaksunT() {
		return chaksunT;
	}

	public void setChaksunT(int chaksunT) {
		this.chaksunT = chaksunT;
	}

	public int getChaksunY() {
		return chaksunY;
	}

	public void setChaksunY(int chaksunY) {
		this.chaksunY = chaksunY;
	}

	public int getChaksun_6m() {
		return chaksun_6m;
	}

	public void setChaksun_6m(int chaksun_6m) {
		this.chaksun_6m = chaksun_6m;
	}

	public int getChulNo() {
		return chulNo;
	}

	public void setChulNo(int chulNo) {
		this.chulNo = chulNo;
	}

	public String getHrName() {
		return hrName;
	}

	public void setHrName(String hrName) {
		this.hrName = hrName;
	}

	public int getHrNo() {
		return hrNo;
	}

	public void setHrNo(int hrNo) {
		this.hrNo = hrNo;
	}

	public String getJkName() {
		return jkName;
	}

	public void setJkName(String jkName) {
		this.jkName = jkName;
	}

	public int getJkNo() {
		return jkNo;
	}

	public void setJkNo(int jkNo) {
		this.jkNo = jkNo;
	}

	public String getMeet() {
		return meet;
	}

	public void setMeet(String meet) {
		this.meet = meet;
	}

	public int getOrd1CntT() {
		return ord1CntT;
	}

	public void setOrd1CntT(int ord1CntT) {
		this.ord1CntT = ord1CntT;
	}

	public int getOrd1CntY() {
		return ord1CntY;
	}

	public void setOrd1CntY(int ord1CntY) {
		this.ord1CntY = ord1CntY;
	}

	public int getOrd2CntT() {
		return ord2CntT;
	}

	public void setOrd2CntT(int ord2CntT) {
		this.ord2CntT = ord2CntT;
	}

	public int getOrd2CntY() {
		return ord2CntY;
	}

	public void setOrd2CntY(int ord2CntY) {
		this.ord2CntY = ord2CntY;
	}

	public String getOwName() {
		return owName;
	}

	public void setOwName(String owName) {
		this.owName = owName;
	}

	public int getOwNo() {
		return owNo;
	}

	public void setOwNo(int owNo) {
		this.owNo = owNo;
	}

	public String getPrd() {
		return prd;
	}

	public void setPrd(String prd) {
		this.prd = prd;
	}

	public String getRating() {
		return rating;
	}

	public void setRating(String rating) {
		this.rating = rating;
	}

	public int getRcCntT() {
		return rcCntT;
	}

	public void setRcCntT(int rcCntT) {
		this.rcCntT = rcCntT;
	}

	public int getRcCntY() {
		return rcCntY;
	}

	public void setRcCntY(int rcCntY) {
		this.rcCntY = rcCntY;
	}

	public Date getRcDate() {
		return rcDate;
	}

	public void setRcDate(Date rcDate) {
		this.rcDate = rcDate;
	}

	public int getRcNo() {
		return rcNo;
	}

	public void setRcNo(int rcNo) {
		this.rcNo = rcNo;
	}

	public String getSex() {
		return sex;
	}

	public void setSex(String sex) {
		this.sex = sex;
	}

	public String getTrName() {
		return trName;
	}

	public void setTrName(String trName) {
		this.trName = trName;
	}

	public int getTrNo() {
		return trNo;
	}

	public void setTrNo(int trNo) {
		this.trNo = trNo;
	}

	public int getWgBudam() {
		return wgBudam;
	}

	public void setWgBudam(int wgBudam) {
		this.wgBudam = wgBudam;
	}
	
	
	
}
