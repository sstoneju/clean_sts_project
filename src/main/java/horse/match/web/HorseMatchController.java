package horse.match.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import horse.match.jpa.HorseMatch;
import horse.match.jpa.HorseMatchRepository;

@Controller
public class HorseMatchController {
	
	@Autowired
	private HorseMatchRepository horseMatchRepository;
	
	@RequestMapping("/horse/match")
	public String view(Model model) {
		// 페이지 별로 호출을 해야하는데. 일단은 그냥 전체를 호출하자.
		List<HorseMatch> matchList = horseMatchRepository.findAll();
		model.addAttribute("MatchList", matchList);
		
		return "horse/match";
	}
}
