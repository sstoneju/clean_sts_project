package horse.main.web;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HorseMainController {
	
	private static final Logger logger = LoggerFactory.getLogger(HorseMainController.class);
	
	
	@RequestMapping(value = "/horse")
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);
		
		Date date = new Date();
		DateFormat dateFormat = DateFormat.getDateTimeInstance(DateFormat.LONG, DateFormat.LONG, locale);
		
		String formattedDate = dateFormat.format(date);
		
		model.addAttribute("serverTime", formattedDate );
		
		return "horse/main";
	}
	
	// 여기에는 메인 페이지에 경기정보를 호출패야하니까
	// 여기다 제일큰 데이터를 가지고 오면 되겠다.
}
